package com.hk.dyeyuesystem.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author：alpha
 * @Create：2021/12/11/17:55
 * @Description：TODO 日期时间工具类
 * @Version：1.0
 */
public class DateUtil {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 日期转字符串
     * yyyy-MM-dd HH:mm:ss
     * @param date 日期
     * @return
     */
    public static String dateString(Date date) {
        String format = simpleDateFormat.format(date);
        return format;
    }

    /**
     * 字符串转日期
     * @param s
     * @return
     */
    public static Date stringDate(String s) {
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            System.err.println("字符串转换日期失败，请检查字符串是否符合转换标准！");
        }
        return date;
    }

    /**
     * 日期转字符串
     * @param date 日期
     * @param s 转换格式 例 yyyy-MM-dd、yyyy/MM/dd、 HH:mm:ss
     * @return
     */
    public static String dateString(Date date,String s) {
        simpleDateFormat = new SimpleDateFormat(s);
        String format = simpleDateFormat.format(date);
        return format;
    }


}
