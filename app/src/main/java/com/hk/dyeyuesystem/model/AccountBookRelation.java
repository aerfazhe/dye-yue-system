package com.hk.dyeyuesystem.model;

import java.util.Date;

/**
 * @Author：alpha
 * @Create：2021/12/4/19:04
 * @Description：借阅/归还记录
 * @Version：1.0
 */
public class AccountBookRelation {

    /**
     * ID 唯一 （默认自增）
     */
    private Integer id;

    /**
     * 账户ID
     */
    private Integer aid;

    /**
     * 书籍ID
     */
    private Integer bid;

    /**
     * 状态 0 已预约 1：借阅中 2：已归还  3：已处理
     */
    private Integer status;

    /**
     * 归还时间（预定归还时间）
     */
    private Date returnTime;

    private String returnTimeString;

    /**
     * 创建时间，借出时间
     */
    private Date createTime;

    /**
     * 修改时间
     * 作为真正归还时间处理
     */
    private Date updateTime;


    public AccountBookRelation(Integer id, Integer aid, Integer bid, Integer status, Date returnTime, Date createTime, Date updateTime) {
        this.id = id;
        this.aid = aid;
        this.bid = bid;
        this.status = status;
        this.returnTime = returnTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public AccountBookRelation(Integer aid, Integer bid, Integer status, String returnTimeString) {
        this.aid = aid;
        this.bid = bid;
        this.status = status;
        this.returnTimeString = returnTimeString;
    }

    public String getReturnTimeString() {
        return returnTimeString;
    }

    public void setReturnTimeString(String returnTimeString) {
        this.returnTimeString = returnTimeString;
    }

    public AccountBookRelation() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AccountBookRelation{" +
                "id=" + id +
                ", aid=" + aid +
                ", bid=" + bid +
                ", status=" + status +
                ", returnTime=" + returnTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
