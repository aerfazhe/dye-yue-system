package com.hk.dyeyuesystem.util;

/**
 * @Author：alpha
 * @Create：2021/12/25/16:57
 * @Description：SQl语句拼接
 * @Version：1.0
 */
public class SqlUtil {

    private static String LIKE = "%";

    public static String likeJoint(String s) {
        return SqlUtil.LIKE+s+SqlUtil.LIKE;
    }

}
