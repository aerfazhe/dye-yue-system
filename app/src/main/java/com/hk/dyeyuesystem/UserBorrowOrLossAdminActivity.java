package com.hk.dyeyuesystem;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.hk.dyeyuesystem.db.DBOpenHelper;
import com.hk.dyeyuesystem.model.AccountBookRelation;
import com.hk.dyeyuesystem.model.Book;
import com.hk.dyeyuesystem.model.LossRecord;
import com.hk.dyeyuesystem.model.vo.AccountBookRelationVO;
import com.hk.dyeyuesystem.model.vo.LossRecordVO;
import com.hk.dyeyuesystem.util.DateUtil;
import com.hk.dyeyuesystem.util.ToastUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 相关账号的借阅记录或挂失记录及相关操作
 */
public class UserBorrowOrLossAdminActivity extends AppCompatActivity {

//    操作状态
    private final static String OPERATE_STATUS_BORROW = "1";
    private final static String OPERATE_STATUS_LOSS = "2";

//    借阅状态
    public final static String BORROW_STATUS_0 = "已预约";
    public final static String BORROW_STATUS_1 = "借阅中";
    public final static String BORROW_STATUS_2 = "已归还";
    public final static String BORROW_STATUS_3 = "已逾期";

//    挂失状态
    public final static String LOSS_STATUS_0 = "挂失中";
    public final static String LOSS_STATUS_1 = "已处理";
    public final static String LOSS_STATUS_2 = "已归还";




    //  Initialize Toast
    private Toast toast;

    //    声明DBOpenHelper对象
    private DBOpenHelper dbOpenHelper;

    private ListView queryBorrowLossListView;

    //   operateStatus 操作状态 1：借阅记录 2、挂失记录
    private String account, operateStatus;

    //    静态归还日期或挂失日期标题
    private TextView recordDateTitle;

    //    当前记录的绑定账号ID
    private String accountId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_borrow_or_loss_admin);
        queryBorrowLossListView = findViewById(R.id.query_borrow_loss_list_view);
        recordDateTitle = findViewById(R.id.record_date_title);
        //        实例化
        dbOpenHelper = new DBOpenHelper(UserBorrowOrLossAdminActivity.this, "db_book_system", null, 1);
        SQLiteDatabase readableDatabase = dbOpenHelper.getReadableDatabase();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        operateStatus = bundle.getString("operateStatus");
        account = bundle.getString("account");

//                借阅记录选项
        if (OPERATE_STATUS_BORROW.equals(operateStatus)) {
            queryBorrowDiaLog(account, readableDatabase);
        }

//                挂失记录选项
        if (OPERATE_STATUS_LOSS.equals(operateStatus)) {
            queryLossDiaLog(account, readableDatabase);
        }

        //        列表添加事件
//        添加长按点击弹出选项框菜单
        queryBorrowLossListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("选择操作！");
//                借阅记录选项
                if (OPERATE_STATUS_BORROW.equals(operateStatus)) {
                    menu.add(0, 0, 0, "借阅中");
                    menu.add(0, 1, 0, "已归还");
                }
//                挂失记录选项
                if (OPERATE_STATUS_LOSS.equals(operateStatus)) {
                    menu.add(0, 2, 0, "已归还");
                    menu.add(0, 3, 0, "已处理");
                }
            }
        });

    }


    /**
     * 查询挂失记录
     *
     * @param accountToString  账号 手机号
     * @param readableDatabase 数据库操作API
     */
    @SuppressLint("LongLogTag")
    private void queryLossDiaLog(String accountToString, SQLiteDatabase readableDatabase) {
        /**
         * 1、通过手机号查询出账号所对应的ID
         * 2、通过ID查询挂失记录表，查看是否具有挂失记录
         *      2.1、有记录
         *          判断目前挂失的状态，并根据状态回显出对应挂失记录
         *    2.2、没有记录
         *          toast提示没有相关挂失记录
         */
        recordDateTitle.setText("挂失日期");
        //        =========================================   步骤1   ==========================================
        accountId = null;
        Cursor account = readableDatabase.query("ACCOUNT", null, "ACCOUNT = ?", new String[]{accountToString}, null, null, null);
        while (account.moveToNext()) {
            accountId = account.getString(0);
        }
//        判断是否存在
        if (accountId == null) {
            toast = ToastUtil.toastHint(toast, UserBorrowOrLossAdminActivity.this, "当前选定用户异常,请检查！");
            return;
        }
//        保存获取的挂失记录
        List<LossRecord> lossRecordList = new ArrayList<>();
        Cursor lossRecord = readableDatabase.query("LOSS_RECORD", null, "AID = ?", new String[]{accountId}, null, null, null);
        while (lossRecord.moveToNext()) {
            Integer aid = null;
            Integer bid = null;
            Integer status = null;
            try {
                aid = Integer.valueOf(lossRecord.getString(1));
                bid = Integer.valueOf(lossRecord.getString(2));
                status = Integer.valueOf(lossRecord.getString(3));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Log.e("UserBorrowOrLossAdminActivity.java", "数据不能转化为数字,请检查！");
            }
            String recordTime = lossRecord.getString(4);
            LossRecord lossRecord1 = new LossRecord(aid, bid, status, recordTime);
            lossRecordList.add(lossRecord1);
        }
        if (lossRecordList.size() == 0 || lossRecordList == null) {
            toast = ToastUtil.toastHint(toast, UserBorrowOrLossAdminActivity.this, "暂无挂失记录!");
            return;
        }
//        具有挂失记录
//        遍历挂失记录，通过图书ID查询图书编号和名称
        List<LossRecordVO> lossRecordVOList = new ArrayList<>();
        for (LossRecord record : lossRecordList) {
//            图书ID
            Integer bid = record.getBid();
//            挂失时间
            String recordTimeToString = record.getRecordTimeToString();
//            挂失状态：0 挂失中 1：已处理 2：已归还
            Integer status2 = record.getStatus();

//             通过图书ID查询图书信息，如果没有，则提示图书已不存在
            Book book = null;
            Cursor query = readableDatabase.query("BOOK", null, "ID = ?", new String[]{bid.toString()}, null, null, null);
            while (query.moveToNext()) {
                String bookNumber = query.getString(1);
                String bookName = query.getString(2);
//                通过构造函数进行封装
                book = new Book(bookNumber, bookName);
            }
            String status = status2 == 0 ? "挂失中" : status2 == 1 ? "已处理" : "已归还";
//            判断该图书是否存在
            if (book == null) {
                LossRecordVO lossRecordVO = new LossRecordVO("0", "*******", recordTimeToString, status);
//                添加借阅记录集合
                lossRecordVOList.add(lossRecordVO);
                continue;
            }
            LossRecordVO lossRecordVO = new LossRecordVO(book.getBookNumber(), book.getBookName(), recordTimeToString, status);
//                添加借阅记录集合
            lossRecordVOList.add(lossRecordVO);
        }
        //        判断挂失记录是否为空
        if (lossRecordVOList.size() <= 0) {
            toast = ToastUtil.toastHint(toast, UserBorrowOrLossAdminActivity.this, "暂时没有挂失记录，请挂失后查看！");
            return;
        }
//        存储获取到的数据
        List<Map<String, Object>> resultList = new ArrayList<>(50);
        for (LossRecordVO lossRecordVO : lossRecordVOList) {
            Map<String, Object> map = new HashMap<>(8);
//                对数据进行封装
            map.put("bookNumber", lossRecordVO.getBookNumber());
            map.put("bookName", lossRecordVO.getBookName());
            map.put("recordTime", lossRecordVO.getRecordTimeToString());
            map.put("status", lossRecordVO.getStatus());
//                将封装的map集合存储到resulList集合中
            resultList.add(map);
        }
//        进行封装，列表展示
        SimpleAdapter simpleAdapter = new SimpleAdapter(
                UserBorrowOrLossAdminActivity.this,
                resultList,
                R.layout.query_book,
                new String[]{
                        "bookNumber", "bookName",
                        "recordTime", "status"
                },
                new int[]{
                        R.id.query_current_book_number, R.id.query_current_book_name,
                        R.id.query_current_return_time, R.id.query_current_book_status
                }
        );
        queryBorrowLossListView.setAdapter(simpleAdapter);
    }

    /**
     * 查询借阅记录
     *
     * @param accountToString  账号 手机号
     * @param readableDatabase 数据库操作API
     */
    @SuppressLint("LongLogTag")
    private void queryBorrowDiaLog(String accountToString, SQLiteDatabase readableDatabase) {
        /**
         * 1、通过手机号查询出账号所对应的ID
         * 2、通过ID查询借阅记录表，查看是否具有借阅记录
         *      2.1、有记录
         *          查看相关记录是否具有被挂失的，
         *              有，就不进行展示
         *              没有，就进行展示
         *      2.2、没有记录
         *         toast提示没有相关借阅记录
         */
//        =========================================   步骤1   ==========================================
        accountId = null;
        Cursor account = readableDatabase.query("ACCOUNT", null, "ACCOUNT = ?", new String[]{accountToString}, null, null, null);
        while (account.moveToNext()) {
            accountId = account.getString(0);
        }
//        判断是否存在
        if (accountId == null) {
            toast = ToastUtil.toastHint(toast, UserBorrowOrLossAdminActivity.this, "当前选定用户异常,请检查！");
            return;
        }
//      =========================================   步骤2   ==========================================
        String where = "AID = ?";
        String[] s = new String[]{accountId};
//        保存获取的借阅记录
        List<AccountBookRelation> accountBookRelationList = new ArrayList<>();
        Cursor cursor = readableDatabase.query("ACCOUNT_BOOK_RELATION", null, where, s, null, null, null);
        /**
         * 判断是否具有借阅记录
         *  有，进行封装
         *  没有，则进行提示
         */
        while (cursor.moveToNext()) {
            Integer aid = null;
            Integer bid = null;
            Integer status = null;
            try {
                aid = Integer.valueOf(cursor.getString(1));
                bid = Integer.valueOf(cursor.getString(2));
                status = Integer.valueOf(cursor.getString(3));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Log.e("UserBorrowOrLossAdminActivity.java", "数据不能转化为数字,请检查！");
            }
            String returnTime = cursor.getString(4);
//            封装为对象
            AccountBookRelation accountBookRelation = new AccountBookRelation(aid, bid, status, returnTime);
//            添加入借阅记录
            accountBookRelationList.add(accountBookRelation);
        }
        if (accountBookRelationList.size() == 0 || accountBookRelationList == null) {
            toast = ToastUtil.toastHint(toast, UserBorrowOrLossAdminActivity.this, "暂无借阅记录!");
            return;
        }
//        具有借阅记录
        /**
         * 遍历借阅记录，通过图书ID查询所属图书编号和名称并进行封装
         * 用图书归还日期判断当前借阅状态是归还逾期还是借阅中
         *  如果图书归还日期大于或等于今天，证明借阅中，如果小于当天，就证明归还已逾期
         */
//        Initialize List
        List<AccountBookRelationVO> accountBookRelationVOList = new ArrayList<>();
        for (AccountBookRelation accountBookRelation : accountBookRelationList) {
//            账号ID
            Integer aid = accountBookRelation.getAid();
//            图书ID
            Integer bid = accountBookRelation.getBid();
//            预计归还日期
            String returnTimeString = accountBookRelation.getReturnTimeString();
//            图书借阅状态
            Integer status2 = accountBookRelation.getStatus();

//             通过图书ID查询图书信息，如果没有，则提示图书已不存在
            Book book = null;
            Cursor query = readableDatabase.query("BOOK", null, "ID = ?", new String[]{bid.toString()}, null, null, null);
            while (query.moveToNext()) {
                String bookNumber = query.getString(1);
                String bookName = query.getString(2);
//                通过构造函数进行封装
                book = new Book(bookNumber, bookName);
            }
//            判断该图书是否存在
            if (book == null) {
//                当前日期
                long time = new Date().getTime();
                long returnTime = DateUtil.stringDate(returnTimeString).getTime();
                String status = status2 == 1 ? "借阅中" : "已预约";
//                判断当前借阅书籍状态
                if (returnTime < time) {
                    status = "已逾期";
                }
                AccountBookRelationVO accountBookRelationVO = new AccountBookRelationVO("0", "*******", returnTimeString, status);
//                添加借阅记录集合
                accountBookRelationVOList.add(accountBookRelationVO);
                continue;
            }
            //                当前日期
            long time = DateUtil.stringDate(DateUtil.dateString(new Date(), "yyyy-MM-dd")).getTime();
            long returnTime = DateUtil.stringDate(returnTimeString).getTime();
            String status = status2 == 1 ? "借阅中" : "已预约";
//                判断当前借阅书籍状态
            if (returnTime < time) {
                status = "已逾期";
            }
            if (status2 == 2) {
                status = "已归还";
            }
//            判断借阅的书籍是否处于挂失阶段
            /**
             * 1、通过借阅记录额账号ID和图书ID查询是否具有挂失记录
             *  1.1、如果有，则状态修改为挂失状态
             *  1.2、没有则不变
             */
//            挂失状态：0 挂失中 1：已处理 2：已归还
            String lossStatus = "-1";
            Cursor loss_record = readableDatabase.query("LOSS_RECORD", null, "AID = ? AND BID = ?", new String[]{aid.toString(), bid.toString()}, null, null, null);
            while (loss_record.moveToNext()) {
                lossStatus = loss_record.getString(3);
            }
            int parseInt = Integer.parseInt(lossStatus);
//            判断此借阅书籍是否具有挂失记录，有则进行跳过不进行放入借阅记录当中
            if (parseInt != -1) {
                continue;
            }
            AccountBookRelationVO accountBookRelationVO = new AccountBookRelationVO(book.getBookNumber(), book.getBookName(), returnTimeString, status);
//                添加借阅记录集合
            accountBookRelationVOList.add(accountBookRelationVO);
        }

//        判断借阅记录是否为空
        if (accountBookRelationVOList.size() <= 0) {
            toast = ToastUtil.toastHint(toast, UserBorrowOrLossAdminActivity.this, "暂时没有借阅记录，请预约后查看！");
            return;
        }
        //        存储获取到的数据
        List<Map<String, Object>> resultList = new ArrayList<>(50);
        for (AccountBookRelationVO accountBookRelationVO : accountBookRelationVOList) {
            Map<String, Object> map = new HashMap<>(8);
            //                对数据进行封装
            map.put("bookNumber", accountBookRelationVO.getBookNumber());
            map.put("bookName", accountBookRelationVO.getBookName());
            map.put("returnTime", accountBookRelationVO.getReturnTime());
            map.put("status", accountBookRelationVO.getStatus());
//                将封装的map集合存储到resulList集合中
            resultList.add(map);
        }
//        进行封装，列表展示
        SimpleAdapter simpleAdapter = new SimpleAdapter(
                UserBorrowOrLossAdminActivity.this,
                resultList,
                R.layout.query_book,
                new String[]{
                        "bookNumber", "bookName",
                        "returnTime", "status"
                },
                new int[]{
                        R.id.query_current_book_number, R.id.query_current_book_name,
                        R.id.query_current_return_time, R.id.query_current_book_status
                }
        );
        queryBorrowLossListView.setAdapter(simpleAdapter);
    }


    /**
     * 给菜单项添加事件
     *
     * @param item 菜单索引
     * @return
     */
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
//        获取上下文
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//        得到listView中选择的条目绑定的Id
        String id = String.valueOf(info.id);
//        得到列表子节点的视图
        View targetView = info.targetView;
        //                获取单列 某一元素的控制权
        TextView bookNumber = targetView.findViewById(R.id.query_current_book_number);
        TextView bookStatus = targetView.findViewById(R.id.query_current_book_status);
//                获取元素内容
        String bookNumberToString = bookNumber.getText().toString();
        String bookStatusToString = bookStatus.getText().toString();
        //        创建数据库操作API实例
        SQLiteDatabase readableDatabase = dbOpenHelper.getReadableDatabase();
        Log.i("=====ListView列表绑定的ID==", id);
        Log.i("===== 对应的用户ID ==", accountId);
        Log.i("===== 对应的图书编号 ==", bookNumberToString);
        Log.i("===== 对应的图书状态 ==", bookStatusToString);
        switch (item.getItemId()) {
//            借阅记录相关操作
// TODO: 2022/1/2 完成事件操作，更新图书借阅记录状态(已完成)
            case 0: // 借阅中 状态：1
//                更新借阅记录为借阅中
                updateBorrowDiaLog(accountId, bookNumberToString, bookStatusToString, readableDatabase, 1);
                return true;
            case 1: //已归还 状态：2
//                 更新借阅记录为已归还
                updateBorrowDiaLog(accountId, bookNumberToString, bookStatusToString, readableDatabase, 2);
//                return true;
//            挂失记录相关操作
// TODO: 2022/1/2 完成事件操作，更新图书挂失记录状态(已完成)
            case 2: //已归还
//                更新挂失状态为已归还
                updateLossStatus(accountId, bookNumberToString, bookStatusToString, readableDatabase, 2);
                return true;
            case 3: // 已处理
                updateLossStatus(accountId, bookNumberToString, bookStatusToString, readableDatabase, 1);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * 更新挂失记录
     *
     * @param accountId          账户ID
     * @param bookNumberToString 图书编号
     * @param bookStatusToString 图书借阅状态：
     * @param readableDatabase   操作API实例
     * @param status             状态：1：更新为已处理 2：更新为已归还
     */
    private void updateLossStatus(String accountId, String bookNumberToString, String bookStatusToString, SQLiteDatabase readableDatabase, int status) {
//        先判断状态 更新为已处理
        if (status == 1) {
            checkAndHandlerLibrary(accountId,bookNumberToString,bookStatusToString,readableDatabase);
        }
//        更新为已归还
        if (status == 2) {
            checkAndLossReturnLibrary(accountId,bookNumberToString,bookStatusToString,readableDatabase);
        }

    }

    /**
     * 更新挂失记录为已归还 2
     * @param accountId          账户ID
     * @param bookNumberToString 图书编号
     * @param bookStatusToString 图书借阅状态：
     * @param readableDatabase   操作API实例
     */
    private void checkAndLossReturnLibrary(String accountId, String bookNumberToString, String bookStatusToString, SQLiteDatabase readableDatabase) {
//        已归还
        if (LOSS_STATUS_2.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前借阅图书已归还了呦，请勿重复操作！");
            return;
        }
//        已处理
        if (LOSS_STATUS_1.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书已处理过喽，请勿重复操作！");
            return;
        }
//        挂失中
        if (LOSS_STATUS_0.equals(bookStatusToString)) {
//            通过图书编号查找图书ID
            String bookNumber = null;
            Integer amount = 0;
            Cursor book = readableDatabase.query("BOOK", null, "BOOK_NUMBER = ?", new String[]{bookNumberToString}, null, null, null);
            while (book.moveToNext()) {
                bookNumber = book.getString(0);
                try {
                    amount = Integer.valueOf(book.getString(6));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.e("BorrowOrLossAdmin--516行","转换数字失败!");
                }
            }
//            判断该图书是否存在
            if (bookNumber == null) {
                toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"未找到该图书，请检查该图书是否存在！");
                return;
            }
            Log.i("===== 对应的用户ID ==", accountId);
            Log.i("===== 对应的图书ID ==", bookNumber);
            ContentValues contentValues = new ContentValues();
            contentValues.put("STATUS",2);
            contentValues.put("UPDATE_TIME",DateUtil.dateString(new Date()));
            int update = readableDatabase.update("LOSS_RECORD", contentValues, "AID = ? AND BID = ?", new String[]{accountId, bookNumber});
            if (update > 0) {
                toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书归还成功！");
//                刷新挂失记录
                queryLossDiaLog(account, readableDatabase);
//                更新借阅记录为已归还：有更新，没有就不更新
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("STATUS",2);
                contentValues3.put("UPDATE_TIME",DateUtil.dateString(new Date()));
                int update3 = readableDatabase.update("ACCOUNT_BOOK_RELATION", contentValues3, "AID = ? AND BID = ?", new String[]{accountId, bookNumber});
                if (update3 > 0) {
                    Log.i("BorrowOrLossAdmin--540行","更新图书借阅记录成功");
                }
//                新增归还的图书数量
                amount++;
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("NUMBER_REMAINING",amount);
                contentValues2.put("UPDATE_TIME",DateUtil.dateString(new Date()));
//                更新图书剩余数
                int update2 = readableDatabase.update("BOOK", contentValues2, "ID = ?", new String[]{bookNumber});
                if (update2 > 0) {
                    Log.i("BorrowOrLossAdmin--550行","更新图书剩余数成功，当前剩余数："+amount);
                }
                return;
            }
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书归还失败！");
            return;
        }

    }

    /**
     * 更新挂失记录为已处理 1
     * @param accountId          账户ID
     * @param bookNumberToString 图书编号
     * @param bookStatusToString 图书借阅状态：
     * @param readableDatabase   操作API实例
     */
    private void checkAndHandlerLibrary(String accountId, String bookNumberToString, String bookStatusToString, SQLiteDatabase readableDatabase) {
        //        已归还
        if (LOSS_STATUS_2.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前借阅图书已归还了呦，请勿重复操作！");
            return;
        }
//        已处理
        if (LOSS_STATUS_1.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书已处理过喽，请勿重复操作！");
            return;
        }

//        挂失中
        if (LOSS_STATUS_0.equals(bookStatusToString)) {
//            通过图书编号查找图书ID
            String bookNumber = null;
            Cursor book = readableDatabase.query("BOOK", null, "BOOK_NUMBER = ?", new String[]{bookNumberToString}, null, null, null);
            while (book.moveToNext()) {
                bookNumber = book.getString(0);
            }
//            判断该图书是否存在
            if (bookNumber == null) {
                toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"未找到该图书，请检查该图书是否存在！");
                return;
            }
            Log.i("===== 对应的用户ID ==", accountId);
            Log.i("===== 对应的图书ID ==", bookNumber);
            ContentValues contentValues = new ContentValues();
            contentValues.put("STATUS",1);
            contentValues.put("UPDATE_TIME",DateUtil.dateString(new Date()));
            int update = readableDatabase.update("LOSS_RECORD", contentValues, "AID = ? AND BID = ?", new String[]{accountId, bookNumber});
            if (update > 0) {
                toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前挂失图书处理成功！");
//                刷新借阅记录
                queryLossDiaLog(account, readableDatabase);
                //                更新借阅记录为已归还：有更新，没有就不更新
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("STATUS",3);
                contentValues3.put("UPDATE_TIME",DateUtil.dateString(new Date()));
                int update3 = readableDatabase.update("ACCOUNT_BOOK_RELATION", contentValues3, "AID = ? AND BID = ?", new String[]{accountId, bookNumber});
                if (update3 > 0) {
                    Log.i("BorrowOrLossAdmin--540行","更新图书借阅记录成功，当前状态：3：已处理");
                }
                return;
            }
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前挂失图书处理失败！");
            return;
        }
    }

    /**
     * 更新借阅记录
     *
     * @param accountId          账户ID
     * @param bookNumberToString 图书编号
     * @param bookStatusToString 图书借阅状态：
     * @param readableDatabase   操作API实例
     * @param status             状态：1：更新为借阅中 2：更新为已归还
     */
    private void updateBorrowDiaLog(String accountId, String bookNumberToString, String bookStatusToString, SQLiteDatabase readableDatabase, int status) {

//        先判断状态 更新为借阅中
        if (status == 1) {
            checkAndInTheLibrary(accountId,bookNumberToString,bookStatusToString,readableDatabase);
        }
//        更新为已归还
        if (status == 2) {
            checkAndReturnLibrary(accountId,bookNumberToString,bookStatusToString,readableDatabase);
        }
    }

    /**
     * 更新借阅记录为已归还
     * @param accountId 账户ID
     * @param bookNumberToString 图书编号
     * @param bookStatusToString 图书借阅状态
     * @param readableDatabase 操作API实例
     */
    private void checkAndReturnLibrary(String accountId, String bookNumberToString, String bookStatusToString, SQLiteDatabase readableDatabase) {
//        判断图书借阅状态 已逾期 或 借阅中
        if (BORROW_STATUS_1.equals(bookStatusToString) || BORROW_STATUS_3.equals(bookStatusToString)) {
//            通过图书编号查找图书ID
            String bookNumber = null;
            Integer amount = 0;
            Cursor book = readableDatabase.query("BOOK", null, "BOOK_NUMBER = ?", new String[]{bookNumberToString}, null, null, null);
            while (book.moveToNext()) {
                bookNumber = book.getString(0);
                try {
                    amount = Integer.valueOf(book.getString(6));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.e("BorrowOrLossAdmin--642行","转换数字失败!");
                }
            }
//            判断该图书是否存在
            if (bookNumber == null) {
                toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"未找到该图书，请检查该图书是否存在！");
                return;
            }
            Log.i("===== 对应的用户ID ==", accountId);
            Log.i("===== 对应的图书ID ==", bookNumber);
            ContentValues contentValues = new ContentValues();
            contentValues.put("STATUS",2);
            contentValues.put("UPDATE_TIME",DateUtil.dateString(new Date()));
            int update = readableDatabase.update("ACCOUNT_BOOK_RELATION", contentValues, "AID = ? AND BID = ?", new String[]{accountId, bookNumber});
            if (update > 0) {
                toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书归还成功！");
//                刷新借阅记录
                queryBorrowDiaLog(account, readableDatabase);
//                新增归还的图书数量
                amount++;
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("NUMBER_REMAINING",amount);
                contentValues2.put("UPDATE_TIME",DateUtil.dateString(new Date()));
//                更新图书剩余数
                int update2 = readableDatabase.update("BOOK", contentValues2, "ID = ?", new String[]{bookNumber});
                if (update2 > 0) {
                    Log.i("BorrowOrLossAdmin--668行","更新图书剩余数成功，当前剩余数："+amount);
                }
                return;
            }
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书归还失败！");
            return;
        }

//        已归还
        if (BORROW_STATUS_2.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前借阅图书已归还了呦，请勿重复操作！");
            return;
        }

//        已预约
        if (BORROW_STATUS_0.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书还未借出，请勿重复操作！");
            return;
        }
    }

    /**
     *  更新借阅记录为借阅中
     * @param accountId     账户ID
     * @param bookNumberToString 图书编号
     * @param bookStatusToString 图书借阅状态
     * @param readableDatabase 操作API实例
     */
    private void checkAndInTheLibrary(String accountId, String bookNumberToString, String bookStatusToString, SQLiteDatabase readableDatabase) {
//        判断图书借阅状态 已逾期
        if (BORROW_STATUS_3.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前借阅图书已逾期,请尽快联系相关借阅者进行处理！");
            return;
        }
//        已归还
        if (BORROW_STATUS_2.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前借阅图书已归还了呦！");
            return;
        }
//        借阅中
        if (BORROW_STATUS_1.equals(bookStatusToString)) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书已被借出喽，请勿重复操作！");
            return;
        }
//        已预约
        if (BORROW_STATUS_0.equals(bookStatusToString)) {
//            通过图书编号查找图书ID
        String bookNumber = null;
        Cursor book = readableDatabase.query("BOOK", null, "BOOK_NUMBER = ?", new String[]{bookNumberToString}, null, null, null);
        while (book.moveToNext()) {
            bookNumber = book.getString(0);
        }
//            判断该图书是否存在
        if (bookNumber == null) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"未找到该图书，请检查该图书是否存在！");
            return;
        }
        Log.i("===== 对应的用户ID ==", accountId);
        Log.i("===== 对应的图书ID ==", bookNumber);
        ContentValues contentValues = new ContentValues();
        contentValues.put("STATUS",1);
        contentValues.put("UPDATE_TIME",DateUtil.dateString(new Date()));
        int update = readableDatabase.update("ACCOUNT_BOOK_RELATION", contentValues, "AID = ? AND BID = ?", new String[]{accountId, bookNumber});
        if (update > 0) {
            toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书借出成功！");
//                刷新借阅记录
            queryBorrowDiaLog(account, readableDatabase);
            return;
        }
        toast = ToastUtil.toastHint(toast,UserBorrowOrLossAdminActivity.this,"当前图书借出失败！");
        return;
    }

    }


    /**
     * 断开数据库连接
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbOpenHelper != null) {
            dbOpenHelper.close();
        }
    }

}