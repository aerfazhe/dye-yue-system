package com.hk.dyeyuesystem;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @Author：alpha
 * @Create：2021/11/28/23:08
 * @Description：个人借书查询相关操作控制层
 * @Version：1.0
 */
public class SubscribeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
    }

}
