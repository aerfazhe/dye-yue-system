package com.hk.dyeyuesystem;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.dyeyuesystem.db.DBOpenHelper;
import com.hk.dyeyuesystem.util.DateUtil;
import com.hk.dyeyuesystem.util.ToastUtil;

import java.util.Date;

/**
 * 修改密码相关操作
 */
public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {

    //    Initialize Toast
    private Toast toast;

    //    声明DBOpenHelper对象
    private DBOpenHelper dbOpenHelper;

//    用户昵称
    private TextView loginName;
//    用户输入的旧密码、新密码、确认密码
    private EditText loginAccount,oldPassword,newPassword,againInputPassword;
//    修改按钮
    private Button updatePPasswordBtn;

//    登录ID
    private String loginId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

//        实例化
        dbOpenHelper = new DBOpenHelper(UpdateActivity.this,"db_book_system",null,1);

        updatePPasswordBtn = findViewById(R.id.update_password_btn);
//        绑定点击事件
        updatePPasswordBtn.setOnClickListener(this);

//        将账号和用户昵称通过bundle从上一个Activity带过来
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        loginId = bundle.getString("loginId");
        String userName = bundle.getString("loginName");
        String account = bundle.getString("loginAccount");
//                获取当前页面昵称和账号区域控制权
        loginName = findViewById(R.id.login_name);
        loginAccount = findViewById(R.id.login_account);
        loginName.setText(userName);
        loginAccount.setText(account);
//        获取旧密码和新密码控制权
        oldPassword = findViewById(R.id.old_password);
        newPassword = findViewById(R.id.new_password);
        againInputPassword = findViewById(R.id.again_input_password);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_password_btn:
                SQLiteDatabase readableDatabase = dbOpenHelper.getReadableDatabase();
//                修改密码判断校验
                checkUpdatePaasword(readableDatabase,loginId);
                break;
            default:
                Toast.makeText(UpdateActivity.this,"待开发！",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * 校验并修改密码
     * @param readableDatabase 数据库操作API
     * @param loginId 登录账号ID
     */
    private void checkUpdatePaasword(SQLiteDatabase readableDatabase, String loginId) {
//                提取输入的密码
        String oldPasswordToString = oldPassword.getText().toString();
        String newPasswordToString = newPassword.getText().toString();
        String againInputPasswordToString = againInputPassword.getText().toString();
//        校验不能为空
        if (oldPasswordToString.equals("") || newPasswordToString.equals("") || againInputPasswordToString.equals("")) {
            toast = ToastUtil.toastHint(toast,UpdateActivity.this,"必传参数不能为空或NULL,请检查！");
            return;
        }

//        通过账号ID查询账号旧密码
        String oldPassword = null;
        Cursor cursor = readableDatabase.query("ACCOUNT", null, "ID = ?", new String[]{loginId}, null, null, null);
        while (cursor.moveToNext()) {
            String string = cursor.getString(3);
            oldPassword = string;
        }
//        校验旧密码
        if (oldPassword == null) {
            toast = ToastUtil.toastHint(toast,UpdateActivity.this,"查询账号信息异常,请检查！");
            return;
        }
        if (!oldPasswordToString.equals(oldPassword)) {
            toast = ToastUtil.toastHint(toast,UpdateActivity.this,"旧密码输入错误,请检查！");
            return;
        }
//        校验新密码
        if (!newPasswordToString.equals(againInputPasswordToString)) {
            toast = ToastUtil.toastHint(toast,UpdateActivity.this,"密码输入不一致,请检查！");
            return;
        }

        if (!checkPassword(newPasswordToString) || !checkPassword(againInputPasswordToString)) {
            toast = ToastUtil.toastHint(toast, UpdateActivity.this, "密码长度不能少于6位,请重新输入！");
            return;
        }

//      执行修改密码
        ContentValues contentValues = new ContentValues();
        contentValues.put("PASSWORD",newPasswordToString);
        contentValues.put("UPDATE_TIME", DateUtil.dateString(new Date()));
        int update = readableDatabase.update("ACCOUNT", contentValues, "ID = ?", new String[]{loginId});
        if (update == 1) {
            toast = ToastUtil.toastHint(toast,UpdateActivity.this,"密码已修改,请重新登录！");
            return;
        }
        toast = ToastUtil.toastHint(toast,UpdateActivity.this,"密码修改失败,请检查！");
        return;


    }

    /**
     * 校验密码是否小于6位数
     * @param pwdToString
     * @return
     */
    private Boolean checkPassword(String pwdToString) {
        if (pwdToString.length() < 6) {
            return false;
        }
        return true;
    }


    /**
     * 断开数据库连接
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbOpenHelper != null) {
            dbOpenHelper.close();
        }
    }

}