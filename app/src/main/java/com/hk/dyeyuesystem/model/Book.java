package com.hk.dyeyuesystem.model;

import java.util.Date;

/**
 * 图书
 */
public class Book {

    /**
     * ID 唯一 （默认自增）
     */
    private Integer id;

    /**
     * 图书编号 唯一
     */
    private String bookNumber;

    /**
     * 图书名称 唯一
     */
    private String bookName;

    /**
     * 作者
     */
    private String auhtor;

    /**
     * 出版社
     */
    private String press;

    /**
     * 总数
     */
    private Integer bookNum;

    /**
     * 剩余数量
     */
    private Integer numberRemaining;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    public Book(Integer id, String bookNumber, String bookName, String auhtor, String press, Integer bookNum, Integer numberRemaining, Date createTime, Date updateTime) {
        this.id = id;
        this.bookNumber = bookNumber;
        this.bookName = bookName;
        this.auhtor = auhtor;
        this.press = press;
        this.bookNum = bookNum;
        this.numberRemaining = numberRemaining;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Book(Integer id, String bookName, String auhtor, String press, Integer bookNum, Integer numberRemaining) {
        this.id = id;
        this.bookName = bookName;
        this.auhtor = auhtor;
        this.press = press;
        this.bookNum = bookNum;
        this.numberRemaining = numberRemaining;
    }

    public Book(String bookNumber, String bookName, String auhtor, String press, Integer bookNum, Integer numberRemaining) {
        this.bookNumber = bookNumber;
        this.bookName = bookName;
        this.auhtor = auhtor;
        this.press = press;
        this.bookNum = bookNum;
        this.numberRemaining = numberRemaining;
    }

    public Book(String bookNumber, String bookName) {
        this.bookNumber = bookNumber;
        this.bookName = bookName;
    }

    public Book() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuhtor() {
        return auhtor;
    }

    public void setAuhtor(String auhtor) {
        this.auhtor = auhtor;
    }

    public String getPress() {
        return press;
    }

    public void setPress(String press) {
        this.press = press;
    }

    public Integer getbookNum() {
        return bookNum;
    }

    public void setbookNum(Integer bookNum) {
        this.bookNum = bookNum;
    }

    public Integer getNumberRemaining() {
        return numberRemaining;
    }

    public void setNumberRemaining(Integer numberRemaining) {
        this.numberRemaining = numberRemaining;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookNumber='" + bookNumber + '\'' +
                ", bookName='" + bookName + '\'' +
                ", auhtor='" + auhtor + '\'' +
                ", press='" + press + '\'' +
                ", bookNum=" + bookNum +
                ", numberRemaining=" + numberRemaining +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
