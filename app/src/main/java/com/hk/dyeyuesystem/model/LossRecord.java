package com.hk.dyeyuesystem.model;

import java.util.Date;

/**
 * @Author：alpha
 * @Create：2021/12/4/20:56
 * @Description：挂失记录
 * @Version：1.0
 */
public class LossRecord {

    /**
     * ID 唯一（默认自增）
     */
    private Integer id;

    /**
     * 账户ID
     */
    private Integer aid;

    /**
     * 书籍ID
     */
    private Integer bid;

    /**
     * 状态 0 挂失中 1：已处理 2：已归还
     */
    private Integer status;

    /**
     * 挂失时间
     */
    private Date recordTime;

    private String recordTimeToString;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    public String getRecordTimeToString() {
        return recordTimeToString;
    }

    public void setRecordTimeToString(String recordTimeToString) {
        this.recordTimeToString = recordTimeToString;
    }

    public LossRecord(Integer aid, Integer bid, Integer status, String recordTimeToString) {
        this.aid = aid;
        this.bid = bid;
        this.status = status;
        this.recordTimeToString = recordTimeToString;
    }

    public LossRecord(Integer id, Integer aid, Integer bid, Integer status, Date recordTime, Date createTime, Date updateTime) {
        this.id = id;
        this.aid = aid;
        this.bid = bid;
        this.status = status;
        this.recordTime = recordTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public LossRecord() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "LossRecord{" +
                "id=" + id +
                ", aid=" + aid +
                ", bid=" + bid +
                ", status=" + status +
                ", recordTime=" + recordTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
