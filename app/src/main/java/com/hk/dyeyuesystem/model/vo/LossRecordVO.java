package com.hk.dyeyuesystem.model.vo;

/**
 * @Author：alpha
 * @Create：2021/12/4/20:56
 * @Description：挂失记录 返回值
 * @Version：1.0
 */
public class LossRecordVO {

    /**
     * 图书编号 唯一
     */
    private String bookNumber;

    /**
     * 图书名称 唯一
     */
    private String bookName;

    /**
     * 挂失时间
     */
    private String recordTimeToString;

    /**
     * 状态
     */
    private String status;

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getRecordTimeToString() {
        return recordTimeToString;
    }

    public void setRecordTimeToString(String recordTimeToString) {
        this.recordTimeToString = recordTimeToString;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LossRecordVO(String bookNumber, String bookName, String recordTimeToString, String status) {
        this.bookNumber = bookNumber;
        this.bookName = bookName;
        this.recordTimeToString = recordTimeToString;
        this.status = status;
    }

    public LossRecordVO() {
        super();
    }

    @Override
    public String toString() {
        return "LossRecordVO{" +
                "bookNumber='" + bookNumber + '\'' +
                ", bookName='" + bookName + '\'' +
                ", recordTimeToString='" + recordTimeToString + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
