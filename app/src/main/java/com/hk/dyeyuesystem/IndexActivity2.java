package com.hk.dyeyuesystem;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hk.dyeyuesystem.util.ToastUtil;

import java.util.Objects;

/**
 * 管理员登录首页相关操作控制层
 */
public class IndexActivity2 extends AppCompatActivity implements View.OnClickListener {

    private Intent intent;

    //    定义一个常量，记录两次点击后退按钮的时间差
    private Long exitTime = 0L;

    //    Initialize Toast
    private Toast toast;

    //    查看
    private ImageButton bookQueryBtn;
    //    预约
    private ImageButton bookSubscribeBtn;
    //    挂失
    private ImageButton bookLossBtn;
    //    修改
    private ImageButton userUpdateBtn;
    //    注销
    private ImageButton logoutBtn;
    //    关于
    private ImageButton inRegardToBtn;
    //    管理
    private ImageButton adminBtn;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index2);
        //        获取绑定按钮
        bookQueryBtn = findViewById(R.id.book_query_btn);
        bookSubscribeBtn = findViewById(R.id.book_subscribe_btn);
        bookLossBtn = findViewById(R.id.book_loss_btn);
        userUpdateBtn = findViewById(R.id.user_update_btn);
        logoutBtn = findViewById(R.id.logout_btn);
        inRegardToBtn = findViewById(R.id.in_regard_to_btn);
        adminBtn = findViewById(R.id.admin_btn);
//        绑定点击事件
        bookQueryBtn.setOnClickListener(this);
        bookSubscribeBtn.setOnClickListener(this);
        bookLossBtn.setOnClickListener(this);
        userUpdateBtn.setOnClickListener(this);
        logoutBtn.setOnClickListener(this);
        inRegardToBtn.setOnClickListener(this);
        adminBtn.setOnClickListener(this);

//        校验是否登录
        checkLogin();

    }


    /**
     * 校验是否登录
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void checkLogin() {
//                提取出Intent当中的Bundle对象，判断当前是否已登录
        Intent intent1 = getIntent();
        Bundle bundle = intent1.getExtras();
        if (Objects.isNull(bundle)) {
            intent = new Intent(IndexActivity2.this,MainActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        if (Objects.nonNull(bundle)) {
            String loginType = (String) bundle.get("loginType");
            if (Objects.isNull(loginType)) {
                intent = new Intent(IndexActivity2.this,MainActivity.class);
                startActivity(intent);
                finish();
                return;
            }
//        判断是否为学生登录
            if (loginType.equals("0")) {
//            指向管理员首页
                intent = new Intent(IndexActivity2.this,IndexActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        }
    }

    /**
     * 点击触发事件
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.book_query_btn:
//                跳转到查询页面
                intent = new Intent(IndexActivity2.this,QueryActivity.class);
//                提取出Intent当中的Bundle对象，放入下一个指向Activity的意图中
                Intent intent1 = getIntent();
                Bundle bundle = intent1.getExtras();
//                存入下一个意图当中
                intent.putExtras(bundle);
//                启动Activity
                startActivity(intent);
                break;
            case R.id.book_subscribe_btn:
//                跳转到预约页面
                intent = new Intent(IndexActivity2.this,BorrowActivity.class);
//                提取出Intent当中的Bundle对象，放入下一个指向Activity的意图中
                Intent intent4 = getIntent();
                Bundle bundle4 = intent4.getExtras();
//                存入下一个意图当中
                intent.putExtras(bundle4);
//                启动Activity
                startActivity(intent);
                break;
            case R.id.book_loss_btn:
//                跳转到挂失页面
                intent = new Intent(IndexActivity2.this,LossActivity.class);
//                提取出Intent当中的Bundle对象，放入下一个指向Activity的意图中
                Intent intent5 = getIntent();
                Bundle bundle5 = intent5.getExtras();
//                存入下一个意图当中
                intent.putExtras(bundle5);
//                启动Activity
                startActivity(intent);
                break;
            case R.id.user_update_btn:
//                跳转到修改页面
                intent = new Intent(IndexActivity2.this,UpdateActivity.class);
//                提取出Intent当中的Bundle对象（当前用户登录信息），放入下一个指向Activity的意图中
                Intent intent2 = getIntent();
                Bundle bundle2 = intent2.getExtras();
//                存入下一个意图当中
                intent.putExtras(bundle2);
//                启动Activity
                startActivity(intent);
                break;
            case R.id.logout_btn:
//                退出登录 注销
                finish();
//                跳转到登录页面
                intent = new Intent(IndexActivity2.this,MainActivity.class);
//                启动Activity
                startActivity(intent);
                break;
            case R.id.in_regard_to_btn:
//                跳转到关于页面
                intent = new Intent(IndexActivity2.this,InRegardToActivity.class);
//                启动Activity
                startActivity(intent);
                break;
            case R.id.admin_btn:
                //                跳转到管理页面
                intent = new Intent(IndexActivity2.this,AdminActivity.class);
//                提取出Intent当中的Bundle对象（当前用户登录信息），放入下一个指向Activity的意图中
                Intent intent3 = getIntent();
                Bundle bundle3 = intent3.getExtras();
//                存入下一个意图当中
                intent.putExtras(bundle3);
//                启动Activity
                startActivity(intent);
                break;
            default:
                Toast.makeText(IndexActivity2.this,"待开发！",Toast.LENGTH_SHORT).show();
                break;
        }
    }


    //    重写onKeyDown()方法来拦截用户单击后退按钮事件
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        判断按下的是否为后退按键
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //    创建退出方法
    private void exit() {
//        判断两次点击的时间差 大于2秒钟
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            toast = ToastUtil.toastHint(this.toast, IndexActivity2.this, "再按一次退出程序！");
            exitTime = System.currentTimeMillis();
        } else {
//            关闭当前Activity
            finish();
//            关闭程序
            System.exit(0);
        }
    }

}