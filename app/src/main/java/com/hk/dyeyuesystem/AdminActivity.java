package com.hk.dyeyuesystem;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Admin
 * 管理员管理界面操作控制层
 */
public class AdminActivity extends AppCompatActivity implements View.OnClickListener {

//    Initialize Intent
    Intent intent;
//    管理员管理相关按钮控件
    private Button bookAdminBtn,userAdminBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
//        获取按钮控制权
        bookAdminBtn = findViewById(R.id.book_admin_btn);
        userAdminBtn = findViewById(R.id.user_admin_btn);
//        添加点击事件
        bookAdminBtn.setOnClickListener(this);
        userAdminBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.book_admin_btn:
//                指向图书管理界面
                intent = new Intent(AdminActivity.this,BookAdminActivity.class);

                startActivity(intent);
                break;
            case R.id.user_admin_btn:
//                指向用户管理界面
                intent = new Intent(AdminActivity.this,UserAdminActivity.class);
                //                提取出Intent当中的Bundle对象（当前用户登录信息），放入下一个指向Activity的意图中
                Intent intent1 = getIntent();
                Bundle bundle = intent1.getExtras();
//                存入下一个意图当中
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}