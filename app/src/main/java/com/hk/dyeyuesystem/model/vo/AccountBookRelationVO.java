package com.hk.dyeyuesystem.model.vo;

/**
 * @Author：alpha
 * @Create：2021/12/25/16:26
 * @Description：借阅记录 返回值
 * @Version：1.0
 */
public class AccountBookRelationVO {


    /**
     * 图书编号 唯一
     */
    private String bookNumber;

    /**
     * 图书名称 唯一
     */
    private String bookName;

    /**
     * 归还时间（预定归还时间）
     */
    private String returnTime;

    /**
     * 状态
     */
    private String status;

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AccountBookRelationVO(String bookNumber, String bookName, String returnTime, String status) {
        this.bookNumber = bookNumber;
        this.bookName = bookName;
        this.returnTime = returnTime;
        this.status = status;
    }

    public AccountBookRelationVO() {
        super();
    }

    @Override
    public String toString() {
        return "AccountBookRelationVO{" +
                "bookNumber='" + bookNumber + '\'' +
                ", bookName='" + bookName + '\'' +
                ", returnTime=" + returnTime +
                ", status='" + status + '\'' +
                '}';
    }

}
