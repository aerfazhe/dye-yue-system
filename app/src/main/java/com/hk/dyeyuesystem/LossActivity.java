package com.hk.dyeyuesystem;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.dyeyuesystem.db.DBOpenHelper;
import com.hk.dyeyuesystem.util.ToastUtil;

/**
 * 图书挂失 控制层
 */
public class LossActivity extends AppCompatActivity implements View.OnClickListener {

    //    Initialize Toast
    private Toast toast;

    //    声明DBOpenHelper对象
    private DBOpenHelper dbOpenHelper;

    private EditText account;
    private EditText password;
    private Button bookLossBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loss);
//        数据库连接实例化
        dbOpenHelper = new DBOpenHelper(LossActivity.this,"db_book_system",null,1);
//        获取组件控制权
        account = findViewById(R.id.account);
        password = findViewById(R.id.password);
        bookLossBtn = findViewById(R.id.book_loss_btn);

        //        将账号和用户昵称通过bundle从上一个Activity带过来
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String accountToString = bundle.getString("loginAccount");
//        进行账号渲染
        account.setText(accountToString);
//        绑定点击事件
        bookLossBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.book_loss_btn:
//                获取输入的账户和密码
                String accountToString = account.getText().toString();
                String passwordToString = password.getText().toString();
                /**
                 * 判断输入是否正确且和数据库存储的内容一致
                 *      1、一致，查询该账户下当前借阅的书籍（未归还的）
                 *      2、提示该账户密码错误或账户不存在
                 */
                SQLiteDatabase readableDatabase = dbOpenHelper.getReadableDatabase();
//                校验账号登录并查询已解约的书籍（包含已逾期）
                checkLoginAndBook(accountToString,passwordToString,readableDatabase);
                break;
            default:
                Toast.makeText(LossActivity.this,"不符合规则点击！",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * 校验账号登录并查询相关借阅记录（已挂失的不显示）
     * @param accountToString 账号
     * @param passwordToString 密码
     * @param readableDatabase 数据库操作API
     */
    private void checkLoginAndBook(String accountToString, String passwordToString, SQLiteDatabase readableDatabase) {
//        校验密码
        if (!checkPassword(passwordToString)) {
            toast = ToastUtil.toastHint(toast,LossActivity.this,"密码长度不能少于6位,请重新输入！");
            return;
        }
//        登录ID
        String loginId = null;
        Cursor cursor = readableDatabase.query("ACCOUNT", null, "ACCOUNT = ? AND PASSWORD = ? ", new String[]{accountToString, passwordToString}, null, null, null);
        while (cursor.moveToNext()) {
            String string = cursor.getString(0);
            loginId = string;
        }
        if (loginId == null) {
            toast = ToastUtil.toastHint(toast,LossActivity.this,"登录失败,请检查密码是否正确！");
            return;
        }
        toast = ToastUtil.toastHint(toast,LossActivity.this,"登录成功！");
//        登录校验成功，指向挂失书籍选择页面
        Intent intent = new Intent(LossActivity.this, LossBookActivity.class);
        //                使用bundle存储登录成功的账号、昵称
        Bundle bundle = new Bundle();
        bundle.putString("loginId",loginId);
        intent.putExtras(bundle);
        startActivity(intent);
        return;


    }

    /**
     * 校验密码是否小于6位数
     * @param pwdToString
     * @return
     */
    private Boolean checkPassword(String pwdToString) {
        if (pwdToString.length() < 6) {
            return false;
        }
        return true;
    }

    /**
     * 断开数据库连接
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbOpenHelper != null) {
            dbOpenHelper.close();
        }
    }

}