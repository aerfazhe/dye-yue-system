package com.hk.dyeyuesystem.util;

import android.content.Context;
import android.widget.Toast;

/**
 * @Author：alpha
 * @Create：2021/12/5/21:31
 * @Description：Toast工具类
 * @Version：1.0
 */
public class ToastUtil {

    /**
     * toast 提示
     * @param toast Toast
     * @param context 上下文
     * @param msg 提示信息
     * @return 返回Toast
     */
    public static Toast toastHint(Toast toast, Context context, String msg) {
        if (toast != null) {
            toast.cancel(); //清除上一次提示
            toast = null;
        }

        if (toast == null) {
            toast =Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.show();
            return toast;
        }
        return null;

    }



}
