package com.hk.dyeyuesystem.model;

import java.util.Date;

/**
 * @Author：alpha
 * @Create：2021/12/4/18:57
 * @Description：账户信息
 * @Version：1.0
 */
public class Account {

    /**
     * ID 唯一 （默认自增）
     */
    private Integer id;

    /**
     * 昵称 唯一
     */
    private String username;

    /**
     * 账号 唯一
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 类型 0： 学生 1：管理员
     */
    private Integer type;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    public Account(Integer id, String username, String account, String password, Integer type, Date createTime, Date updateTime) {
        this.id = id;
        this.username = username;
        this.account = account;
        this.password = password;
        this.type = type;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }



    public Account() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", type=" + type +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
