package com.hk.dyeyuesystem;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hk.dyeyuesystem.db.DBOpenHelper;
import com.hk.dyeyuesystem.model.Account;
import com.hk.dyeyuesystem.util.CustomEditTextDialog;
import com.hk.dyeyuesystem.util.ToastUtil;

import java.util.Objects;

/**
 * 登录相关控制层
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

//    Initialize Toast
    private Toast toast;

//    声明DBOpenHelper对象
    private DBOpenHelper dbOpenHelper;

    //    定义一个常量，记录两次点击后退按钮的时间差
    private Long exitTime = 0L;

    //    Initialize intent
    private Intent intent;
    //    Initialize spinner
    private Spinner userType;

    //    Initialize user account password type
    private String account;
    private String password;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        对应的xml布局文件
        setContentView(R.layout.activity_main);
//        实例化
        dbOpenHelper = new DBOpenHelper(MainActivity.this,"db_book_system",null,1);

//        获取登录按钮并设置事件
        Button loginBtn = findViewById(R.id.login_btn);
        Button registerBtn = findViewById(R.id.register_btn);
        Button adminBtn = findViewById(R.id.admin_btn);
        loginBtn.setOnClickListener(this);
        registerBtn.setOnClickListener(this);
        adminBtn.setOnClickListener(this);
//         登录账户类型
        userType = findViewById(R.id.user_type);
        userType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = (String) userType.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    /**
     * 一个Activity多个事件的时候，实现View.OnclickListener接口的onClick方法
     *
     * @param v
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            指向登录
            case R.id.login_btn:
                login();
                break;
//            指向注册界面
            case R.id.register_btn:
                intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
                //                    关闭当前登录界面
//                finish();
                break;
//            指向弹窗，经确认后指向注册
            case R.id.admin_btn:
//                管理员注册入口
                adminRegister();
                break;

        }
    }

    /**
     * 管理员注册入口
     */
    private void adminRegister() {
        //                记录点击确认按钮次数
        final int[] sureCount = {0};
//                设置一个对话框
        final CustomEditTextDialog editTextDialog = new CustomEditTextDialog(MainActivity.this);
//                获取对话框中输入框的控制权
        final EditText editText = (EditText) editTextDialog.getEditText();
//                点击确认后的操作
        editTextDialog.setOnSureListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toString = editText.getText().toString();
//                        管理员注册码
                String adminCode = "RanYue123456";
                if (toString.equals(adminCode)) {
//                            注册码输入正确，指向注册界面
                    intent = new Intent(MainActivity.this, RegisterActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("1","1");
                    intent.putExtras(bundle);
//                            开始跳转
                    startActivity(intent);
//                    关闭当前登录界面
//                    finish();
                    editTextDialog.dismiss();
                    return;
                }
                if (sureCount[0] < 5) {
                    toast = ToastUtil.toastHint(toast,MainActivity.this,"注册码输入错误!");
                    sureCount[0]++;
                    return;
                }
                toast = ToastUtil.toastHint(toast,MainActivity.this,"注册码输入次数已达上限,请稍后再试!");
                return;
            }
        });
//                点击取消后的操作
        editTextDialog.setOnCanlceListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast = ToastUtil.toastHint(toast,MainActivity.this,"已取消本次操作！");
//                        弹框消失
                editTextDialog.dismiss();
            }
        });
        editTextDialog.setTitle("请输入管理员注册码！");
        editTextDialog.show();
    }


    /**
     * 登录逻辑
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void login() {
        //        获取登录账号和密码
        account = ((EditText) findViewById(R.id.account)).getText().toString();
        password = ((EditText) findViewById(R.id.password)).getText().toString();
//        登录账号类型
        String userType = type.equals("学生") ? "0" : type.equals("管理员") ? "1" : "2";
//        获取到数据库操作对象SQLiteDatabase
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
        /**
         * 开始账号认证 表名 ，查询的列，查询所有null，查询条件where
         */
//      执行查询操作
        Cursor cursor = database.query("ACCOUNT", null, "ACCOUNT = ? AND PASSWORD = ? AND TYPE = ?", new String[]{account, password, userType}, null, null, null);
//        登录用户信息
        Account account = null;
//        判断是否具有登录信息，有 遍历，没有 跳过
        while (cursor.moveToNext()) {
            account = new Account();
//            列，按从左到右排序，索引0开始
            String id = cursor.getString(0);
            String username = cursor.getString(1);
            String loginAccount = cursor.getString(2);
            String loginType = cursor.getString(4);
//            封装登录用户信息
            account.setId(Integer.parseInt(id));
            account.setUsername(username);
            account.setAccount(loginAccount);
            account.setType(Integer.parseInt(loginType));
        }
//        判断是否登录成功
        if (Objects.nonNull(account)) {
//            0：学生 1：管理员
            if (account.getType() == 0) {
                toast = ToastUtil.toastHint(this.toast, MainActivity.this, "登录成功！");
                //                意图，指向另一个Activity 学生首页界面
                intent = new Intent(MainActivity.this, IndexActivity.class);
//                使用bundle存储登录成功的账号、昵称
                Bundle bundle = new Bundle();
                bundle.putString("loginId",account.getId().toString());
                bundle.putString("loginAccount", account.getAccount());
                bundle.putString("loginName", account.getUsername());
                bundle.putString("loginType", "0");
//            将bundle保存到intent中进行Activity之间传递
                intent.putExtras(bundle);
//                启动Activity
                startActivity(intent);
//                    关闭当前登录界面
                finish();
                return;
            } else if (account.getType() == 1) {
                toast = ToastUtil.toastHint(this.toast, MainActivity.this, "登录成功！");
                //        指向 管理员界面
                intent = new Intent(MainActivity.this, IndexActivity2.class);
                //                使用bundle存储登录成功的账号、昵称
                Bundle bundle = new Bundle();
                bundle.putString("loginId",account.getId().toString());
                bundle.putString("loginAccount", account.getAccount());
                bundle.putString("loginName", account.getUsername());
                bundle.putString("loginType", "1");
//            将bundle保存到intent中进行Activity之间传递
                intent.putExtras(bundle);
//                启动Activity
                startActivity(intent);
//                    关闭当前登录界面
                finish();
                return;
            }
            toast = ToastUtil.toastHint(this.toast, MainActivity.this, "登录失败！请检查账号/密码/类型");
            return;
        }
        toast = ToastUtil.toastHint(this.toast, MainActivity.this, "登录失败！请检查账号/密码/类型");
    }


    //    重写onKeyDown()方法来拦截用户单击后退按钮事件
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        判断按下的是否为后退按键
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //    创建退出方法
    private void exit() {
//        判断两次点击的时间差 大于2秒钟
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            toast = ToastUtil.toastHint(this.toast, MainActivity.this, "再按一次退出程序！");
            exitTime = System.currentTimeMillis();
        } else {
//            关闭当前Activity
            finish();
//            关闭程序
            System.exit(0);
        }
    }

    /**
     * 断开数据库连接
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbOpenHelper != null) {
            dbOpenHelper.close();
        }
    }
}