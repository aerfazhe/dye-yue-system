package com.hk.dyeyuesystem;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @Author：alpha
 * @Create：2021/11/28/23:20
 * @Description：过往借阅控制层
 * @Version：1.0
 */
public class LePassActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_le_pass);
    }

    @Override
    public void onClick(View v) {

    }
}
