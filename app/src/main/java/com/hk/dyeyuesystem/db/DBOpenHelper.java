package com.hk.dyeyuesystem.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * @Author：alpha
 * @Create：2021/12/5/16:27
 * @Description：数据库连接操作类
 * @Version：1.0
 */
public class DBOpenHelper extends SQLiteOpenHelper {

//    定义创建表ACCOUNT（账号）的sql语句常量
    final String CREATE_TABLE_ACCOUNT_SQL = "CREATE TABLE IF NOT EXISTS \"ACCOUNT\" (\n" +
        "  \"ID\" integer NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
        "  \"USERNAME\" text NOT NULL,\n" +
        "  \"ACCOUNT\" text NOT NULL,\n" +
        "  \"PASSWORD\" text NOT NULL,\n" +
        "  \"TYPE\" integer NOT NULL,\n" +
        "  \"CREATE_TIME\" text NOT NULL,\n" +
        "  \"UPDATE_TIME\" text NOT NULL\n" +
        ")";
//    创建表BOOK（图书）的sql的语句
    final String CREATE_TABLE_BOOK_SQL = "CREATE TABLE IF NOT EXISTS \"BOOK\" (\n" +
            "  \"ID\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
            "  \"BOOK_NUMBER\" text NOT NULL,\n" +
            "  \"BOOK_NAME\" TEXT NOT NULL,\n" +
            "  \"AUTHOR\" TEXT NOT NULL,\n" +
            "  \"PRESS\" TEXT NOT NULL,\n" +
            "  \"BOOK_NUM\" INTEGER NOT NULL,\n" +
            "  \"NUMBER_REMAINING\" INTEGER NOT NULL DEFAULT 0,\n" +
            "  \"CREATE_TIME\" TEXT NOT NULL,\n" +
            "  \"UPDATE_TIME\" TEXT NOT NULL\n" +
            ")";
//    创建借阅记录表SQL
    final String CREATE_TABLE_ACCOUNT_BOOK_RELATION_SQL = "CREATE TABLE IF NOT EXISTS \"ACCOUNT_BOOK_RELATION\" (\n" +
            "  \"ID\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
            "  \"AID\" INTEGER NOT NULL,\n" +
            "  \"BID\" INTEGER NOT NULL,\n" +
            "  \"STATUS\" integer NOT NULL DEFAULT 0,\n" +
            "  \"RETURN_TIME\" TEXT NOT NULL,\n" +
            "  \"CREATE_TIME\" TEXT NOT NULL,\n" +
            "  \"UPDATE_TIME\" TEXT NOT NULL\n" +
            ")";

//    创建挂失记录报表SQL
    final String CREATE_TABLE_LOSS_RECORD_SQL = "CREATE TABLE IF NOT EXISTS \"LOSS_RECORD\" (\n" +
            "  \"ID\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
            "  \"AID\" INTEGER NOT NULL,\n" +
            "  \"BID\" INTEGER NOT NULL,\n" +
            "  \"STATUS\" integer NOT NULL,\n" +
            "  \"RECORD_TIME\" TEXT NOT NULL,\n" +
            "  \"CREATE_TIME\" TEXT NOT NULL,\n" +
            "  \"UPDATE_TIME\" TEXT NOT NULL\n" +
            ")";

    public DBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, version);


    }

    /**
     * 创建表
     * @param db 当前sqlite库
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
//        执行SQl创建相关表
        db.execSQL(CREATE_TABLE_ACCOUNT_SQL);
        db.execSQL(CREATE_TABLE_BOOK_SQL);
        db.execSQL(CREATE_TABLE_ACCOUNT_BOOK_RELATION_SQL);
        db.execSQL(CREATE_TABLE_LOSS_RECORD_SQL);
    }

    /**
     * 版本更新
     * @param db 当前的数据库
     * @param oldVersion 旧版本
     * @param newVersion 新版本
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("染悦图书管理系统","---版本更新"+oldVersion+"--->"+newVersion);

    }
}
