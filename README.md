# DyeYueSystem

#### 介绍
染悦图书管理系统

#### 软件架构
软件架构说明

采用Java作为后台进行开发

#### 大致预览

##### 登录界面

<img src="README.assets/1641214836258.png" alt="1641214836258" style="zoom:67%;" />

##### 账号注册界面

<img src="README.assets/1641214898536.png" alt="1641214898536" style="zoom:67%;" />

##### 首页

<img src="README.assets/1641214928308.png" alt="1641214928308" style="zoom:67%;" />

##### 部分其他页面

<img src="README.assets/1641214962795.png" alt="1641214962795" style="zoom:67%;" />





#### 使用说明

1.  下载运行即可
注意该项目gradle版本为6.1.1

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
